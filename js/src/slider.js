/**
 * slider.js
 *
 * Copyright (c) 2015 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Christopher Guindon (Eclipse Foundation)- initial API and implementation
 */
import 'rangeslider.js'

// Has to use jQuery as rangeslider.js only work with it
(function( jQuery, window, document ) {

  let output = document.querySelectorAll('.donate-amount')[0];
  let form_amount = document.querySelectorAll('.form-amount')[0];

  $(document).on('input', 'input[type="range"]', function(e) {
    if (e.currentTarget.value == 1) {
      output.value = 5;
      form_amount.value = 5;
    }
    if (e.currentTarget.value == 2) {
      output.value = 10;
      form_amount.value = 10;
    }
    if (e.currentTarget.value == 3) {
      output.value = 35;
      form_amount.value = 35;
    }
    if (e.currentTarget.value == 4) {
      output.value = 50;
      form_amount.value = 50;
    }
    if (e.currentTarget.value == 5) {
      output.value = 100;
      form_amount.value = 100;
    }
  });

  $('input.donate-amount').focusout(function() {
    if ($(this).val() < 1) {
      $(this).val(1);
    }
    $('input.form-amount').val($(this).val());
  });

  $('input[type="range"]').rangeslider({
    polyfill: false
  });


})(jQuery, window, document );
