---
title: "Projects"
seo_title: "Projects - Eclipse IDE Working Group"
content_classes: "padding-top-50"
hide_sidebar: true
container: "container"
hide_page_title: true
---

{{< page-projects >}}

{{< eclipsefdn_projects
    templateId="tpl-projects-item"
    url="https://projects.eclipse.org/api/projects?working_group=eclipse-ide"
    classes="margin-top-30"
    display_categories="false"
    categories=""
    display_view_more="false"
>}}
