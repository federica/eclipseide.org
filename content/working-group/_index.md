---
title: "Eclipse IDE Working Group"
seo_title: "Eclipse IDE Working Group"
hide_sidebar: true
hide_page_title: true
date: 2022-08-15
layout: single
---

{{< about >}}

{{< members >}}

{{< sponsors >}}

{{< grid/div class="featured-section featured-news-events row" isMarkdown="false"  >}}
  {{< grid/div class="container" isMarkdown="false" >}}
    {{< grid/div class="row" isMarkdown="false" >}}
      {{< grid/div class="col-sm-12 featured-news-block" isMarkdown="false" >}}
        {{< grid/div class="featured-news" isMarkdown="false" id="news" >}}
          <h2>News</h2>
          {{< newsroom/news id="news-template-id" templateId="custom-news-template" templatePath="/js/templates/news-home.mustache" id="news-list-container" containerClass="match-height-item-by-row" count="2" class="news-list-custom" publishTarget="eclipse_ide" includeList="true">}}
        {{</ grid/div >}}
      {{</ grid/div >}}
      {{< grid/div class="col-sm-12 text-center featured-events-block" isMarkdown="false" >}}
        {{< grid/div class="featured-events" isMarkdown="false" >}}
          <h2>Upcoming Events</h2>
          {{< newsroom/events
            id="event-list-container"
            publishTarget="eclipse_ide"
            class="events-list-custom"
            containerClass="match-height-item-by-row"
            upcoming="1"
            templateId="custom-events-template"
            templatePath="/js/templates/event-list-format.mustache"
            includeList="true"
            count="4"
          >}}
        {{</ grid/div >}}
      {{</ grid/div >}}
    {{</ grid/div >}}
   {{</ grid/div >}}
{{</ grid/div >}}

