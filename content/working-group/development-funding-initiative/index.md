---
title: Helping to Assure the Future of the Eclipse IDE
description:
  The IDE Working Group has funds available to developers to resolve top
  technical priorities
keywords: ["funding", "development", "initiative", "future"]
container: container margin-top-20 margin-bottom-20
hide_sidebar: true
---

The IDE Working Group has funds available to developers to resolve top technical
priorities, identified by the Planning Council.

## Background

At the Eclipse Foundation, open source projects progress using developers who
are employed by members, self-employed, or volunteers. The Eclipse IDE working
group was formed to ensure the continued sustainability, integrity, evolution,
and adoption of the Eclipse IDE & RCP suite of products and related
technologies.

Since it was formed in June of 2021 the working group has been sponsoring the
release engineering, development of key features and fixes that everyone
benefits from. Indeed, one of the main reasons for forming an industry
collaboration in support of the Eclipse IDE was to address 'the tragedy of the
commons', and to secure funding for and to engage directly with developers who
are capable of fixing known common problems. The way it works is the Planning
Council identifies the top technical priorities to the Steering Committee and
then the Eclipse Foundation locates an individual(s) to perform the development
work arranging a required services agreement.

## Guiding Principles

- Adhere to the principles of transparency and openness.
- Complement the existing community development efforts.
- Encourage a "multiplying effect" where community participation is amplified by
  this funding program's undertakings. To ensure the funds allocated fit within
  the overall working group program plan and budget.

We follow the
[Guidelines for Management of Working Group Funded Development Efforts and Initiatives](https://www.eclipse.org/org/workinggroups/wgfi_program.php).

## Committed Funds

Total working group funds committed to date:

{{< pages/working-group/development-funding-initiative/committed_funds_table >}}

We are actively recruiting new members and sponsors to the working group so we
are optimistic the program funding for 2023 will grow.

## Follow Our Progress

The IDE Working Group Development of the Commons GitLab Group is where detailed
information about the efforts realized to date and those which are in progress
and/or planned. The top issues are tracked on a
[GitLab Board](https://gitlab.eclipse.org/eclipse-wg/ide-wg/ide-wg-dev-funded-efforts/ide-wg-dev-funded-program-planning-council-top-issues/-/boards/1208).

## Get Involved

Are you a developer that is interested in entering into a service agreement to
take on one of the Planning Council's top technical priorities? If this appeals
to you, please contact <collaborations@eclipse-foundation.org>.

Join or sponsor the Eclipse IDE working group, learn more about our
[members and corporate sponsors](/working-group/#members). Or you can
[contribute funds personally](https://www.eclipse.org/sponsor/) to support the
work being done by this initiative.
